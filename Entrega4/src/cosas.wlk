/*Knight Rider : pesa 500 kilos y su nivel de peligrosidad es 10 */
object knightRider {
	var peso = 500
	var peligrosidad = 10

	method peso() {
		return peso
	}
	method peligrosidad() {
		return peligrosidad
	}
}
/*Bumblebee: pesa 800 kilos y su nivel de peligrosidad es 15 si est� transformado en auto o 30 si est� como robot. */
object bumblebee {
	var peso = 800
	var peligrosidad = 15

	method peso() {
		return peso
	}
	method peligrosidad() {
		return peligrosidad
	}
	method tranformarRobot() {
		peligrosidad = 30
	}
	method transformarAuto() {
		peligrosidad = 15
	}
}
object ladrillo {
	var peso = 2
	method peso() {
		return peso
	}
	method peligrosidad() {
		return 0
	}
}
/*Paquete de ladrillos : cada ladrillo pesa 2 kilos, la cantidad de ladrillos que tiene puede variar. La peligrosidad es 2 */
object paqueteDeLadrillos {
	var ladrillos = [ ]
	var peligrosidad = 2
	method agregarLadrillos(ladrillo) {
		ladrillos.add(ladrillo)
	}
	method peso() {
		return ladrillos.sum({ ld => ld.peso() })
	}
	method peligrosidad() {
		return peligrosidad
	}
}
/*Arena a granel : El peso es variable. La peligrosidad es 1 */
object arenaAGranel {
	var peso = 0
	var peligrosidad = 1
	method agregarArena(_pesoEnKg) {
		peso = _pesoEnKg
	}
	method peso() {
		return peso
	}
	method peligrosidad() {
		return peligrosidad
	}
}
/*Bater�a antia�rea : El peso es 300 kilos si est� con los misiles o 200 en otro caso.
En cuanto a la peligrosidad es 100 si est� con los misiles y 0 en otro caso. */
object bateriaAntiaerea {
	var peso = 200
	var misiles = [ ]
	var peligrosidad = 0

	method agregarMisil(misil) {
		misiles.add(misil)
	}
	method peso() {
		if (misiles.isEmpty()) {
			return peso
		} return peso + misiles.get(0).peso()
	}
	method peligrosidad() {
		if (misiles.isEmpty()) {
			return peligrosidad
		} return peligrosidad + misiles.get(0).peligrosidad()
	}
}

object misil {
	var peso = 100
	var peligrosidad = 100
	method peso() {
		return peso
	}
	method peligrosidad() {
		return peligrosidad
	}
}
/*Contenedor portuario: Un contenedor puede tener otras cosas adentro. El peso es
100 + la suma de todas las cosas que est� adentro. Es tan peligroso como el objeto
m�s peligroso que contiene. Si est� vac�o es 0. */
object contenedorPortuario {
	var peso = 100
	var cosas = [ ]
	method peso() {
		return peso + cosas.sum({ cosa => cosa.peso() })
	}
	method peligrosidad() {
		return cosas.max({ cosa => cosa.peligrosidad() })
	}
	method agregarCosa(_cosa) {
		cosas.add(_cosa)
	}
}

/*Residuos radioactivos : El peso es variable y su peligrosidad es 200 */
object residuosRadioactivos {
	var peso = 0
	var peligrosidad = 200

	method peso() {
		return peso
	}
	method peligrosidad() {
		return peligrosidad
	}

	method establecerPeso(_peso) {
		peso = _peso
	}
}

/*Embalaje de seguridad : Es una cobertura que envuelve a cualquier otra cosa. El
peso es el peso de la cosa que tenga adentro. El nivel de peligrosidad es la mitad
del nivel de peligrosidad de lo que envuelve. */
object embalajeDeSeguridad {
	var cosa

	method peso() {
		if (cosa==null) {
			return 0
		} return cosa.peso()
	}
	method peligrosidad() {
		if (cosa==null) {
			return 0
		} return cosa.peligrosidad() / 2
	}

	method envalar(_cosa) {
		cosa = _cosa
	}
}