import cosas.*
object camion {
	var cosas = [ ]
	var tara = 1000
	var pesoMaximo = 2000

	method cargar(cosa) {
		cosas.add(cosa)
	}
	method cargarTodo(_cosas){
		//Otra opcion es hacer _cosas.forEach({cosita => self.cargar(cosita)})
		cosas.addAll(_cosas)
	}
	method descargar(cosa) {
		cosas.remove(cosa)
	}
	/* Es la suma del peso del cami�n vac�o (tara) y su carga. la tara del method cami�n es de 1 tonelada. */
	method pesoTotal() {
		return tara + cosas.sum({ cosa => cosa.peso() })
	}
	/* Si el peso total es superior al peso m�ximo. El cual es de 2.5 toneladas*/
	method excedidoDePeso() {
		return self.pesoTotal() > pesoMaximo
	}
	/* Todos los objetos cargados que superan el nivel de peligrosidad n. */
	method objetosPeligrosos(n) {
		return cosas.filter({cosa => cosa.peligrosidad() > n})
	}
	/*: Todos los objetos cargados que son m�s peligrosos que la cosa. */
	method objetosMasPeligrososQue(cosa) {
		return cosas.filter({cosita => cosita.peligrosidad() > cosa.peligrosidad()})
	}
	/* Puede circular si ning�na cosa que transporta supera el nivelMaximoPeligrosidad. */
	method puedeCircularEnRuta(nivelMaximoPeligrosidad) {
		return !cosas.any({cosa => cosa.peligrosidad()<nivelMaximoPeligrosidad})
	}
}