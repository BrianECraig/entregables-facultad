/*
 * Agregar al modelo los viajes. De cada viaje nos interesa: los kil�metros, el tiempo m�ximo de viaje
 *  en horas, la cantidad de pasajeros, y tambi�n un conjunto de colores incompatibles, o sea, que los 
 * pasajeros rechazan hacer el viaje en autos de esos colores.
 * 
 * Agregar la capacidad de preguntar si un auto puede hacer un viaje, envi�ndole un
 * mensaje al viaje, claro, con el auto como par�metro. Para que un auto pueda hacer
 * un viaje se tienen que dar tres condiciones: 
 * 
* que la velocidad m�xima sea al menos 10 km/h mayor a la velocidad promedio que necesita 
* el viaje (que es kil�metros dividido tiempo m�ximo);
* 
* que la capacidad del auto d� para la cantidad de pasajeros del viaje;
* 
* y que el auto no sea de un color incompatible para el viaje.
* 
* Usando esto, agregarle a las remiser�as un m�todo para que puedan responder qu� autos 
* pueden hacer un viaje.
 */
class Viajes {
	var kilometros
	var tiempoMaximoDeViajeEnHoras
	var cantidadPasajeros
	const coloresIncompatibles = #{ }
	var auto

	constructor (_kilometros,_tiempoMaximoDeViajesEnHoras,_cantidadPasajerso, _coloresIncompatibles){
		kilometros=_kilometros
		tiempoMaximoDeViajeEnHoras = _tiempoMaximoDeViajesEnHoras
		cantidadPasajeros = _cantidadPasajerso
		coloresIncompatibles.addAll(_coloresIncompatibles)
	}
	
	method auto()
	method kilometros()
	
	method puedeHacerElViaje(_auto) {
		return _auto.velocidadMaxima() > (self.velocidadPromedio()+10)
				&& _auto.capacidad() >= cantidadPasajeros
				&& !coloresIncompatibles.contains(_auto.color())
	}
	
	method velocidadPromedio(){
		return kilometros / tiempoMaximoDeViajeEnHoras
	}
	
	method asignarAuto(_auto){
		auto = _auto
	}
	
	method lugaresLibres() {
		return cantidadPasajeros - auto.capacidad()
	}
}