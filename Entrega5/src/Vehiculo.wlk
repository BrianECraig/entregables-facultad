
class Vehiculo {
	var capacidad
	var velocidadMaxima
	var color
	var peso

	constructor(_capacidad, _velocidadMaxima, _color, _peso) {
		capacidad = _capacidad
		velocidadMaxima = _velocidadMaxima 
		color = _color
		peso = _peso
	}
	
	method color(){
		return color
	}
	method peso(){
		return peso
	}
	method velocidadMaxima(){
		return velocidadMaxima
	}
	method capacidad(){
		return capacidad
	}
	
}