import Vehiculo.*
import colores.*

class AutoStandard inherits Vehiculo {
	var tanqueAdicional

	constructor(_tanqueAdicional) =
	//paso null en el constructor del padre xq no me acepta un parametro de "tipo" object
	super (0 , 0 , null, 1200 ) {
		tanqueAdicional = _tanqueAdicional 
		color = azul
	}
	
	override method velocidadMaxima(){
		if (tanqueAdicional) {
			return 120
		} else {
			return 110
		}
	}
	
	override method capacidad() {
		if (tanqueAdicional) {
			return 3
		} else {
			return 4
		}
	}
	
	override method peso(){
		if (tanqueAdicional) {
			return peso + 150
		} else {
			return peso
		}
	}
		
}