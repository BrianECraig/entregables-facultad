import Remiseria.*
import Vehiculo.*
import colores.*
import Viajes.*
import ChevroletCorsa.*
import AutoStandard.*
import Trafic.*

class Remiseria {
	const flota = #{}
	const viajesHechos = #{}
	var valorMinimo
	var valorKilometro

	constructor(_valorMinimo, _valorKilometro) {
		valorMinimo = _valorMinimo valorKilometro = _valorKilometro
	}

	method agregarAFlota(vehiculo) {
		flota.add(vehiculo)
	}
	method quitarDeFlota(vehiculo) {
		flota.remove(vehiculo)
	}
	/**, la suma del peso de cada veh�culo agregado a la ota. */
	method pesoTotalFlota() {
		return flota.sum({ v => v.peso() })
	}
	/** es verdadero si la remiser�a tiene al menos 3 veh�culos, y adem�s, 
	* todos los veh�culos registrados en la remiser�a pueden ir al menos a 100 km/h. */
	method esRecomendable() {
		return flota.size() > 3 && flota.all({ v => v.velocidadMaxima() >= 100 })
	}
	/**La cantidad total de personas que puede transportar la remiser�a, 
	* considerando solamente los autos de su flota cuya velocidad m�xima sea mayor 
	* o igual a la velocidad indicada. */
	method capacidadTotalYendoA(velocidad) {
		return flota.filter({ v => v.velocidadMaxima() >= velocidad}).map({ v => v.capacidad() }).sum()
	}
	method colorDelAutoMasRapido() {
		return flota.max({ v => v.velocidadMaxima() }).color()
	}

	method autosQuePuedenHacerViaje(viaje) {
		return flota.filter({ a => viaje.puedeHacerElViaje(a) })
	}

	method registrarViaje(viaje, auto) {
		self.validarViaje(viaje, auto)
		viajesHechos.add(viaje.asignarAuto(auto))

	}

	method validarViaje(viaje, auto) {
		if (! viaje.puedeHacerElViaje(auto)) {
			self.error("El auto no puede hacer el viaje. No cumple las condiciones")
		}
	}

	method cuantosViajesHizo(auto) {
		return self.viajesDeUnAuto(auto).size()
	}

	method cuantosViajesHizoMasKms(kms) {
		return viajesHechos.filter({ v => v.kilometros() == kms }).size()
	}

	method cuantosLugaresLibres() {
		return viajesHechos.sum({ v => v.lugaresLibres() })
	}

	/*cu�nto pagarle a un auto, de acuerdo a los viajes que hizo para esa remiser�a. 
	 * Cada remiser�a establece un valor por kil�metro, y un m�nimo para cada viaje. 
	 * 
	 * Por ejemplo, si una remiser�a establece 3 pesos por km y 30 de m�nimo por viaje, 
	 * 
	 * entonces un viaje de 7 km lo paga 30 pesos (porque 3 x 7 = 21 no llega a 30), 
	 * y un viaje de 25 km lo paga 75 pesos (3 x 25 = 75 supera el m�nimo de 30). */
	method cuantoPagarA(auto) {
		var pago = 0
		self.viajesDeUnAuto(auto)
			.forEach({ viaje => { 
				if (viaje.kilometros() * valorKilometro > valorMinimo) {
					pago += viaje.kilometros() * valorKilometro
				} else {
					pago += valorMinimo
				}
			}})
		return pago
	}

	method viajesDeUnAuto(auto) {
		return viajesHechos.filter({ v => v.auto() == auto })
	}
}