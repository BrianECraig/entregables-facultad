import Vehiculo.*
import colores.*

class Trafic inherits Vehiculo {
	var motor
	var interior

	constructor(_motor, _interior) = super ( 0 , 0 , null , 4000 ) {
		motor = _motor 
		interior = _interior 
		color = blanca
	}
	override method peso(){
		return peso + motor.peso() + interior.peso()
	}
	override method velocidadMaxima(){
		return motor.velocidadMaxima()
	}
	override method capacidad(){
		return interior.capacidad()
	}
}

class Motor {
	var peso
	var velocidadMaxima

	constructor(_peso, _velocidadMaxima) {
		peso = _peso velocidadMaxima = _velocidadMaxima
	}

	method peso() {
		return peso
	}

	method velocidadMaxima() {
		return velocidadMaxima
	}
}

class Interior {
	var peso
	var capacidad

	constructor(_peso, _capacidad) {
		peso = _peso capacidad = _capacidad
	}
	
	method peso() {
		return peso
	}
	
	method capacidad() {
		return capacidad
	}
}