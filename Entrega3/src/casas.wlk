import cuentas.*
import cosas.*

object casaDePepeJulian {
	var cuentaDeCasa = cuentaCombinada 
	var gastos = 0 
	var tieneComida = false
	var equipada = false

	/**Si la suma de todas las gastos supera los 5000 pesos */
	method esDerrochona() {
		return 5000 > gastos
	}
	/**si la casa puede afrontar una compra de 40.000 pesos */
	method esBancan() {
		return 40000 < cuentaDeCasa.saldo()
	}

	/**Se saca la plata de la cuentaDeCasa y se incrementas los gastos. */
	method gastar(unaCantidadPesos) {
		cuentaDeCasa.extraer(unaCantidadPesos)
		gastos += unaCantidadPesos
	}
	
	method comprar(cosa) {
		self.gastar(cosa.precio())
		tieneComida = tieneComida or cosa.esComida()
		equipada = cosa.esElectrodomestico() or ( cosa.precio() > 5000 )
	}
	
	method tieneComida() { return tieneComida }

	method vieneDeEquiparse() { return equipada }

	method puedeComprar(cosa) {
		return cuentaDeCasa.saldo() >= cosa.precio()
	}
	method cuentaParaGastos() {
		return cuentaDeCasa
	}
	method bancaCuenta(unaCuenta) {
		cuentaDeCasa = unaCuenta
	}
}


