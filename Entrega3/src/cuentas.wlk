/**La cuenta de Pepe es una cuenta en pesos normal: el saldo incrementa con los
dep�sitos y disminuye con las extracciones en exactamente la misma cantidad de
pesos que indican los par�metros. */
object cuentaDePepe {
	var saldo = 0

	/**devuelve un n�mero que es el saldo en pesos de la cuenta */
	method saldo() {
		return saldo
	}
	/**incrementa el saldo */
	method depositar(unaCantidadPesos) {
		saldo += unaCantidadPesos
	}
	/**disminuye el saldo */
	method extraer(unaCantidadPesos) {
		saldo -= unaCantidadPesos
	}
}

/**
La cuenta de Juli�n est� embargada, esto signica que de cada dep�sito solo el 80%
incrementa el saldo, el 20% restante se descarta. Por ejemplo, si Juli�n tiene $100
de saldo, y deposita otros $100, el saldo es $180. Adem�s, cada vez vez que extrae
se le descuenta al saldo $5 adicionales en concepto de gastos administrativos. Este
gasto es aplicado solo en aquellos casos en que el gasto no deje la cuenta con saldo
negativo. */
object cuentaDeJulian {
	var saldo = 0

	/**incrementa el saldo */
	method saldo() {
		return saldo
	}

	/**devuelve un n�mero que es el saldo en pesos de la cuenta */
	method depositar(unaCantidadPesos) {
		saldo += unaCantidadPesos * 0.8
	}
	/**disminuye el saldo */
	method extraer(unaCantidadPesos) {
		saldo = ( saldo - unaCantidadPesos - 5 ).max(0)
	}
}

/**
La cuenta del pap� de ambos est� dolarizada: mantiene internamente un saldo en
d�lares. Se usa igual que una cuenta en pesos (se extrae y se depositan pesos), pero
en cada operaci�n hay que convertir la moneda usando los precios de compra y de
venta seg�n corresponda. Los valores iniciales son $15.10 para la venta y $14.70
para la compra. Obviamente, estos valores pueden cambiar con el tiempo. */
object cuentaDePapa {
	var saldo = 0
	var precioDeCompra = 14.70
	var precioDeVenta = 15.10

	/**devuelve un n�mero que es el saldo en pesos de la cuenta */
	method saldo() {
		return saldo * precioDeCompra
	}

	/**incrementa el saldo */
	method depositar(unaCantidadPesos) {
		saldo += unaCantidadPesos / precioDeVenta
	}
	/**disminuye el saldo */
	method extraer(unaCantidadPesos) {
		saldo -= unaCantidadPesos / precioDeCompra
	}
}
/**Juan y Pepe se pusieron de acuerdo para que en ciertas �pocas, afronten los gastos
juntos. Para eso es necesario generar una cuenta combinada, que une otras dos. Tiene
una cuenta primaria y una secundaria. */
object cuentaCombinada {
	var primaria = cuentaDePepe var secundaria = cuentaDeJulian

	method asignarCuentaPrimaria(unaCuenta) {
		primaria = unaCuenta
	}

	method asignarCuentaSecundaria(unaCuenta) {
		secundaria = unaCuenta
	}

	/**devuelve un n�mero que es el saldo en pesos de la cuenta. Devuelve la suma
	 de los saldos de las cuentas primaria y secundaria. */
	method saldo() {
		return primaria.saldo() + secundaria.saldo()
	}
	/**incrementa el saldo. si el saldo de la cuenta secundaria es menor a 1000 
	deposita en dicha cuenta.  En cualquier otro caso deposita en la primaria*/
	method depositar(unaCantidadPesos) {
		if (secundaria.saldo() < 1000) {
			secundaria.depositar(unaCantidadPesos)

		} else {
			primaria.depositar(unaCantidadPesos)
		}
	}
	/**disminuye el saldo. Extrae todo lo que puede de la cuenta primaria y 
	el resto de la secundaria */
	method extraer(unaCantidadPesos) {
		if (primaria.saldo() >= unaCantidadPesos) {
			primaria.extraer(unaCantidadPesos)
		} else {
			primaria.extraer(primaria.saldo())
			secundaria.extraer(unaCantidadPesos - primaria.saldo())
		}
	}
}