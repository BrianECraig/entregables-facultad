object casa {
	var cuenta = pepe
	var compras = []
	method tieneComida(){
		return compras.any({c=> c.esComida()})
	}
	method vieneDeEquiparse(){
		return !compras.isEmpty() and 
			(compras.last().esElectrodomestico() 
			or (compras.last().precio()>5000))
	}
	method compras(){ return compras }
	method puedeComprar(cosa){
		return self.cuentaParaGastos().saldo()>=cosa.precio()
	}
	method cuentaParaGastos(){
		return cuenta		
	}
	method bancaCuenta(unaCuenta){
		cuenta = unaCuenta
	}
	method gastar(importe){
		self.cuentaParaGastos().extraer(importe)
	}
	method comprar(cosa){
		self.gastar(cosa.precio())
		compras.add(cosa)
	}
	method esDerrochona(){
		return compras.sum({c=>c.precio()}) > 5000
	}
	method esBacan(){
		return self.cuentaParaGastos().saldo()>=40000
	}
}

object combinada{
	var primaria
	var secundaria
	method programarCuentas(pri, secu){
		primaria = pri
		secundaria = secu
	}
	method saldo(){
		return primaria.saldo()+secundaria.saldo()
	}
	method depositar(unaCantidadPesos){
		if (secundaria.saldo() < 1000) 
			secundaria.deposito(unaCantidadPesos)
		else primaria.deposito(unaCantidadPesos)
	}
	method extraer(unaCantidadPesos){
		if (unaCantidadPesos>primaria.saldo()){
			secundaria.extraer(unaCantidadPesos-primaria.saldo())			
		}
		primaria.extraer(primaria.saldo().min(unaCantidadPesos))
	}
}

object pepe {
	var saldo = 0
	method saldo(){return saldo}
	method depositar(unaCantidadPesos){
		saldo+=unaCantidadPesos
	}
	method extraer(unaCantidadPesos){
		saldo-=unaCantidadPesos
	}
}

object julian {
	var saldo = 0
	method saldo(){return saldo}
	method depositar(unaCantidadPesos){
		saldo+=unaCantidadPesos*0.8
	}
	method extraer(unaCantidadPesos){
		saldo=(saldo-unaCantidadPesos-5).max(0)
	}	
}

object papa {
	var saldoEnDolares = 0
	var precioDeVenta = 15.10
	var precioDeCompra = 14.70
	method saldo(){
		return saldoEnDolares * precioDeCompra
	}
	method precioDeVenta(unPrecio){
		precioDeVenta = unPrecio
	}
	method precioDeCompra(unPrecio){
		precioDeCompra = unPrecio
	}
	method depositar(unaCantidadPesos){
		saldoEnDolares+=unaCantidadPesos/precioDeVenta
	}
	method extraer(unaCantidadPesos){
		saldoEnDolares-=unaCantidadPesos/precioDeCompra
	}
}

object heladera{
	method precio(){return 20000}
	method esComida(){return false}
	method esElectrodomestico(){return true}
}

object cama{
	method precio(){return 8000}
	method esComida(){return false}
	method esElectrodomestico(){return false}
}

object asado{
	method precio(){return 350}
	method esComida(){return true}
	method esElectrodomestico(){return false}
}

object fideos{
	method precio(){return 50}
	method esComida(){return true}
	method esElectrodomestico(){return false}
}

object plancha{
	method precio(){return 1200}
	method esComida(){return false}
	method esElectrodomestico(){return true}
}