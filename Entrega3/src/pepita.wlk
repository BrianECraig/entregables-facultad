object entrenador {
	var rutina
	var repeticiones
	method rutina(unaRutina) {
		rutina = unaRutina 
	}
	method repeticiones(cantidad) {
		repeticiones = cantidad
	}
	method entrenar(unAve) {
		repeticiones.times(	{rutina.apply(unAve)}	)
	}
}

object roque {
	var ave = pepita
	method entrenar(){
		self.aQuienEntrenas().volar(10)
		self.aQuienEntrenas().comer(300)
		self.aQuienEntrenas().volar(5)
		self.aQuienEntrenas().haceLoQueQuieras()
	}
	method entrenarA(unAve){
		ave = unAve
	}
	method aQuienEntrenas(){return ave}
}

object susana {
	var ave = pepita
	method entrenar(){
		self.aQuienEntrenas().comer(100)
		self.aQuienEntrenas().volar(20)
	}
	method entrenarA(unAve){
		ave = unAve
	}
	method aQuienEntrenas(){return ave}
}

object pepita {
	var energia = 0
	var siEstaCansada = {e => false }
	var accionCansada = { }
	method energia(){return energia	}
	method volar(kms) {	
		if (siEstaCansada.apply(energia)) {
			accionCansada.apply()
		}
		energia -= kms + 10
	}	
	method comer(gramos) { 
		energia += 4 * gramos
	}	
	method estaDebil(){	return energia < 50	}
	method estaFeliz(){	return energia.between(500, 1000) }
	method cuantoQuiereVolar(){
		var km = energia / 5
		if (energia.between(300, 400)) km += 10
		if (energia % 20 == 0) km += 15
		return km
	} 
	method haceLoQueQuieras(){
		if (self.estaDebil()) self.comer(20)
		if (self.estaFeliz()) self.volar(self.cuantoQuiereVolar())
	}
	method cansadaSi(cansada) {
		siEstaCansada = cansada
	}
	method cuandoEstasCansadaHace(hacer) {
		accionCansada = hacer
	}
	method puedeVolar(unosKms){
		return energia >= 10 + unosKms 
	}
}

object pepon {
	var energia = 0
	method energia(){return energia	}
	method volar(kms) {	energia -= 1 + 0.5 * kms }	
	method comer(gramos) { energia += 2 * gramos }	
	method haceLoQueQuieras(){ self.volar(1) }	
	method puedeVolar(unosKms){
		return energia >= 1 + unosKms * 0.5
	}
}

object pipa {
	var km = 0
	var calorias = 0
	method volar(kms) {	km += kms }	
	method comer(gramos) { calorias += gramos }	
	method kmsRecorridos(){ return km }
	method caloriasIngeridas(){ return calorias }
	method haceLoQueQuieras(){ }	
	method puedeVolar(unosKms){ return true }	
}