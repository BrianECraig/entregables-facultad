import Persona.*

/**
* Menor : se sabe quién es la madre. Reside, en cualquier año, en los mismos paises
* que la madre. OJO los viajes del menor son separados, si viajaron juntos, hay
* que cargar el viaje en los dos, solamente se considera que coinciden los países de
* residencia
 */
class Menor inherits Persona {
	const mama

	constructor(_mama) {
		mama = _mama
	}
	override method paisesDondeVivio(_anio) {
		return mama.paisesDondeVivio(_anio)
	}
}