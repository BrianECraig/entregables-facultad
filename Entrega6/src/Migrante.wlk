import Persona.*
/**
 * Migrante: se sabe en qué país nació, a qué país se mudó, y en qué año. Hasta el año
 * antes de mudarse, residió en el país en el que nació. Después de mudarse, residió
 * en el país al que se mudó. El año en que se mudó residió en los dos.
 */
class Migrante inherits Persona {
	const pais
	const paisActual
	const anio

	constructor(_paisNacimiento, _paisActual, _anio) {
		pais = _paisNacimiento 
		paisActual = _paisActual 
		anio = _anio
	}

	override method paisesDondeVivio(_anio) {
		var vivioEn = #{ }
		
		if (_anio <= anio) {
			vivioEn.add(pais)
		} 
		
		if (_anio >= anio) {
			vivioEn.add(paisActual)
		} 
		return vivioEn
	}
}