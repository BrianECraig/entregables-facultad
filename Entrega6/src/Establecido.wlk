import Persona.*

/** Establecido: se sabe en qué país vive, en cualquier año que se pregunte, 
residió en ese país y en ningún otro. */
class Establecido inherits Persona {
	const pais

	constructor(paisEstablecido) {
		pais = paisEstablecido
	}
	override method paisesDondeVivio(_anio) {
		return #{ pais }
	}
} 