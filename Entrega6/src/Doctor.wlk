import Persona.*
/**
 * Doctor : se sabe en qué país vive, en qué país hizo el doctorado, y entre qué años.
 * P.ej. si Juan, que vive en Brasil, hizo el doctorado en Colombia entre 2008 y 2011,
 * entonces residió en Colombia esos 4 años, y en Brasil hasta 2008 inclusive, y a
 * partir de 2011 (para 2009 y 2010, residió todo el año en Colombia).
 */
class Doctor inherits Persona{
	const pais
	const paisDoctorado
	const anioInicioCarrera
	const anoFinCarrera
	
	constructor(_paisNacimiento, _paisDoctorado, _anioInicio, _anioFin){
		pais = _paisNacimiento
		paisDoctorado = _paisDoctorado
		anioInicioCarrera = _anioInicio
		anoFinCarrera = _anioFin
	}
	
	override method paisesDondeVivio(_anio){
		const vivioEn = #{}
		if (_anio<=anioInicioCarrera or _anio>=anoFinCarrera){
			vivioEn.add(pais)
		} 
		if (_anio>=anioInicioCarrera and _anio<=anoFinCarrera){
			vivioEn.add(paisDoctorado)
		} 		
		return vivioEn
	}
}