/**
Modela un conjunto de personas, y para cada persona, los viajes que hizo.
 */
class Persona {
	const viajesQueHizo = #{ }

	method enQuePaisesEstuvo(_anio) {
		return self.paisesDondeVivio(_anio) + self.paisesDondeViajo(_anio)
	}

	method coincidioCon(per, _anio) {
		return self.enQuePaisesEstuvo(_anio).any{ p => per.vivioEn(_anio, p) }
	}

	method addViaje(unViaje) {
		viajesQueHizo.add(unViaje)
	}
	
	method paisesDondeVivio(_anio) 
	
	method paisesDondeViajo(_anio) {
		return viajesQueHizo.filter{ v => v.ano() == _anio }.map{ v => v.pais() }
	}

	method vivioEn(_anio, unPais) {
		return self.enQuePaisesEstuvo(_anio).contains(unPais)
	}
}
